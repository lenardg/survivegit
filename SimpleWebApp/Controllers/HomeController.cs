﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleWebApp.Controllers {
	public class HomeController : Controller {
		public ActionResult Index() {
			return View();
		}

		public ActionResult About() {
			ViewBag.Message = "11.2.2016 Demo app for GIT Survival Guide";

			return View();
		}

		public ActionResult Contact() {
			ViewBag.Message = "Offbeat Solutions Oy";

			return View();
		}
	}
}